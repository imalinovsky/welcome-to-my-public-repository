# README #

Welcome and thanks for stopping by. Its so nice seeing a friendly face. I strongly believe in the power of 
Open Source and community collaboration and contribute to various Open Source projects at my free time. 
However, I cannot open most of my code to public in this repository due to the nature of my employment and my 
commitment to complying with my employer's policies and regulations.



May the Force be with you